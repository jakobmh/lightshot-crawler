DISCLAIMER  
THIS PROGRAM IS INTENDED FOR EDUCATIONAL AND DEMONSTRATIONAL USE ONLY.  
EVERY USER IS RESPONSIBLE FOR THEIR OWN ACTIONS.


# lightshot-crawler
`lightshot-crawler` is a proof-of-concept tool to demonstrate the exploitability of the [Lightshot screenshot sharing tool](https://app.prntscr.com).
## Background
Screenshots shared on Lightshot are available via alphanumerical IDs, e.g `https://prnt.sc/123abc`.  
The ID gets incremented for every image. This makes iterating over images easy.  
(e.g. `123456` -> `123457`, `blobby` -> `blobbz`)

Additionally, some users don't care about sharing confidential information via publicly available screenshots.  
I took this as an invitation to have some fun with it.

This program automatically downloads screenshots for further processing. A built-in text recognition and content scoring feature might come in the future.


*Free screenshots for everyone!*  
*Have fun!*

## Usage

    python3 lightshot-crawler.py [OPTIONS]  

### Options

    -s [ID]     --start=[ID]    specifies the starting ID.  
    -c [COUNT]  --count=[COUNT] specifies the number of IDs to go through. Default: infinite  
    -d [PATH]   --dest=[PATH]   specifies the path in wich the screenshots will be saved. Default: ./downloads  
    -h          --help          displays help information  
